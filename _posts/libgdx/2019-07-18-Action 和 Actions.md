---
title: Action 和 Actions
date: 2019-07-18
categories:
- LibGDX
tags:
- Java
- LibGDX
- Game
---

# 待更新

<!-- more -->

# Actions

`com.badlogic.gdx.scenes.scene2d.actions.Actions`

Actions 类封装了一些常用的演员动作和运动模式，这些方法都是静态方法，方便直接调用，下面列出这些动作的用法和解释

| def                                                                        | m                                                                                 |
| -------------------------------------------------------------------------- | --------------------------------------------------------------------------------- |
| addAction(Action action)                                                   |                                                                                   |
| addAction(Action action, Actor targetActor)                                |                                                                                   |
| addListener(EventListener listener, boolean capture)                       |                                                                                   |
| addListener(EventListener listener, boolean capture, Actor targetActor)    |                                                                                   |
| after(Action action)                                                       |                                                                                   |
| alpha(float a, float duration)                                             | Transitions from the alpha at the time this action starts to the specified alpha. |
| color(Color color, float duration)                                         | Transitions from the color at the time this action starts to the specified color. |
| delay(float duration)                                                      |                                                                                   |
| delay(float duration, Action delayedAction)                                |                                                                                   |
| fadeIn(float duration)                                                     | Transitions from the alpha at the time this action starts to an alpha of 1.       |
| fadeIn(float duration, Interpolation interpolation)                        | Transitions from the alpha at the time this action starts to an alpha of 1.       |
| fadeOut(float duration)                                                    | Transitions from the alpha at the time this action starts to an alpha of 0.       |
| fadeOut(float duration, Interpolation interpolation)                       | Transitions from the alpha at the time this action starts to an alpha of 0.       |
| forever(Action repeatedAction)                                             |                                                                                   |
| hide()                                                                     |                                                                                   |
| layout(boolean enabled)                                                    |                                                                                   |
| moveBy(float amountX, float amountY, float duration)                       |                                                                                   |
| moveTo(float x, float y, float duration)                                   |                                                                                   |
| moveToAligned(float x, float y, int alignment, float duration)             |                                                                                   |
| parallel()                                                                 |                                                                                   |
| parallel(Action... actions)                                                |                                                                                   |
| removeAction(Action action)                                                |                                                                                   |
| removeAction(Action action, Actor targetActor)                             |                                                                                   |
| removeActor()                                                              |                                                                                   |
| removeActor(Actor removeActor)                                             |                                                                                   |
| removeListener(EventListener listener, boolean capture)                    |                                                                                   |
| removeListener(EventListener listener, boolean capture, Actor targetActor) |                                                                                   |
| repeat(int count, Action repeatedAction)                                   |                                                                                   |
| rotateBy(float rotationAmount, float duration)                             |                                                                                   |
| rotateTo(float rotation, float duration)                                   |                                                                                   |
| run(java.lang.Runnable runnable)                                           |                                                                                   |
| scaleBy(float amountX, float amountY, float duration)                      |                                                                                   |
| scaleTo(float x, float y, float duration)                                  |                                                                                   |
| sequence()                                                                 |                                                                                   |
| sequence(Action... actions)                                                |                                                                                   |
| show()                                                                     |                                                                                   |
| sizeBy(float amountX, float amountY, float duration)                       |                                                                                   |
| sizeTo(float x, float y, float duration)                                   |                                                                                   |
| timeScale(float scale, Action scaledAction)                                |                                                                                   |
| touchable(Touchable touchable)                                             |                                                                                   |
| visible(boolean visible)                                                   |                                                                                   |

| Action 模式      |     |     |     |
| -------------- | --- | --- | --- |
| ParallelAction |     |     |     |
| RepeatAction   |     |     |     |
| SequenceAction |     |     |     |

# Action

`com.badlogic.gdx.scenes.scene2d.Action`

# 实例

## 模仿《辐射避难所》开场动画

```java
package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class GameLearn extends ApplicationAdapter {

    public SpriteBatch batch; // 画笔
    public FishActor actor; // 演员

    private Texture titleTexture, ownTexture, logoTexture; // 演员的纹理
    private Image img1, img2, img3; // 开场动画演员
    private Stage stage; // 舞台
    private ScreenViewport viewport; // 屏幕视口
    boolean isPlay;

    @Override
    public void create() {

        batch = new SpriteBatch();
        stage = new Stage();

        isPlay = false;

        logoTexture = new Texture("logo_full.png");
        ownTexture = new Texture("own.png");
        titleTexture = new Texture("title.png");

        img1 = new Image(logoTexture);
        img2 = new Image(ownTexture);
        img3 = new Image(titleTexture);

        img1.setPosition(0, -480);
        img2.setPosition(0, -480);
        img3.setPosition(0, -480);

        stage.addActor(img1);
        stage.addActor(img2);
        stage.addActor(img3);

        setAction(img1, 0);
        setAction(img2, 2);
        setAction(img3, 4);

        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (isPlay) {
            stage.act();
            stage.draw();
        }
    }

    @Override
    public void dispose() {
    }

    public void setAction(final Image image, int waitTime) {

        Action movetowait = Actions.moveTo(0, -480, waitTime); //
        Action moveto1 = Actions.moveTo(0, 0, 0.8f);
        Action moveto2 = Actions.moveTo(0, 0, 1f);
        Action moveto3 = Actions.moveTo(0, 300, 0.8f);
        Action moveto4 = Actions.moveTo(0, 330, 1.2f); // 设置 MoveToAction
        Action moveto5 = Actions.moveTo(0, 480, 0.5f); // 设置 MoveToAction

        // 演员动作结束后，在控制台打印一句话
        Action endAction = Actions.run(new Runnable() {
            @Override
            public void run() {
                System.out.println("演员类所有动作执行结束");
            }
        });

        Action sequenceAction = Actions.sequence(movetowait, moveto1, moveto2, moveto3, moveto4, moveto5, endAction);
        image.addAction(sequenceAction);
    }

}
```

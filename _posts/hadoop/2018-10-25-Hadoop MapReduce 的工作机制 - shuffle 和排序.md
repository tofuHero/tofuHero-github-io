---
title: Hadoop MapReduce 的工作机制 - shuffle 和排序
date: 2018-10-25
categories:
- Hadoop
tags:
- Java
- Hadoop
- MapReduce
- shuffle
---

> 内容摘选来自 《Hadoop 权威指南 第四版》 7.3.1、7.3.2

MapReduce 确保每个 reducer 的输入都是按键排序的。系统执行排序、将 map 输出作为输出传给 reducer 的过程称为 *shuffle*[^shuffle]

<!-- more -->

![MapReduce 的 shuffle 和排序][1]

# map 端

每个 map 任务都有一个环形缓冲区（默认 100M）用于存储任务输出。一旦缓冲内容达到阀值（默认 80%）便会有一个线程开始把内容溢出（spill）到磁盘。在溢出的过程中，map 输出继续写到缓冲区，如果缓冲区被填满，map 会阻塞直到写磁盘过程完成。

在写磁盘之前，线程首先根据数据最终要传的 reducer 会把数据划分成相应的分区（partition）。在每个分区中，后台线程按键进行内存中排序，如果有一个 combiner 函数，它就在排序后的输出上运行。

每次内存缓冲区到达溢出阀值，就会新建一个溢出文件（spill file）。在完成任务之前，溢出文件被合并成一个已分区且排序的输出文件。

如果至少存在 3 个溢出文件时，则 combiner 就会在输出文件写到磁盘之前再次运行。

reducer 端通过 HTTP 得到输出文件的分区。

# reduce 端

map 输出文件位于运行 map 任务的 tasktracker 的本地磁盘。每个 map 任务的完成时间可能不同，因此在每个任务完成时，reduce 任务就开始复制其输出。

如果 map 输出相当小，会被复制到 reduce 任务的 JVM 的内存，否则，map 输出会被复制到磁盘。一旦内存缓冲区达到阈值大小或达到 map 输出阈值，则合并后溢写到磁盘中。如果指定 combiner，则合并期间运行它以降低写入磁盘的数据量。

复制完所有 map 输出后，reduce 任务进入排序（合并）阶段，这个阶段将合并 map 输出，维持其顺序排序。

![通过合并因子 10 有效地合并 30 个文件片段][2]

在 reduce 阶段，对已排序输出中的每个键调用 reduce 函数。输出直接写到输出文件系统（如：HDFS）


[^shuffle]: 事实上，shuffle 这个说法并不准确。因为在某些语境中，它只代表 reduce 任务获取 map 输出的这部分过程。在此将其理解为从 map 产生输出到 reduce 消化输入的整个过程

[1]: /imgs/shuffle.svg
[2]: https://s1.ax2x.com/2018/10/25/5XGyOd.png

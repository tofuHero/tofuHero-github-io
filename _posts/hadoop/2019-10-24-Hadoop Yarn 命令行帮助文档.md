---
title: Hadoop Yarn 命令行帮助文档
date: 2019-10-24
categories:
- Hadoop
tags:
- Java
- Hadoop
- Yarn
- 集群
- 监控
---

![Yarn](/imgs/Yarn.svg)
再生产环境中，有可能无法直接查看 yarn 页面，而页面的信息正是开发人员需要了解的，这时我们可以使用 yarn 提供的命令行来查看相关信息。本篇翻译了 yarn 命令行的帮助文档给需要的人，详细使用方法会稍后补充。

<!-- more -->

# yarn 命令帮助

- 输出帮助信息
```sh
yarn --help
```
- 用法：`yarn [--config confdir] COMMAND`
    
  其中 **COMMAND** 可以为：

| COMMAND                            | 功能描述                             |
| ---------------------------------- | ------------------------------------ |
|resourcemanager -format-state-store | 删除 RMStateStore                    |
|resourcemanager                     | 运行 ResourceManager</br>使用 `-format-state-store` 删除 RMStateStore</br>使用 `-remove-application-from-state-store <appId>` 从 RMStateStore 删除应用 |
|nodemanager                         | 在每个从站上运行一个 nodemanager     |
|timelineserver                      | 运行 timeline 服务                   |
|rmadmin                             | 管理员工具                           |
|version                             | 打印版本                             |
|jar <jar>                           | 运行一个 jar 文件                    |
|application                         | 打印 application(s) 报告/kill 应用   |
|applicationattempt                  | 打印 applicationattempt(s) 报告      |
|container                           | 打印 container(s) 报告               |
|node                                | 打印 node 报告                       |
|queue                               | 打印 queue 信息                      |
|logs                                | 转储容器日志                         |
|classpath                           | 打印 Hadoop jar 和所有必须库的类路径 |
|daemonlog                           | 获取或设置每个守护程序的日志级别     |
|top                                 | 运行集群使用情况工具                 |
|CLASSNAME                           | 运行名为 CLASSNAME 的类              |


# node 节点相关

- 输出帮助信息
```shell
yarn node
```
- 用法 `yarn node <options>`

  其中 **\<options\>** 可以为：

| options          | 功能描述                                                      |
| ---------------- | ------------------------------------------------------------- |
| -all             | 与 -list 一起使用可以显示所有节点                             |
| -list            | 列出所有正在运行的节点。支持指定 -states 来过滤复合条件的节点，指定 -all 可以显示所有节点。 |
| -states <States> | 与 -list 一起使用可以过滤出指定状态的节点，多个状态亿逗号分割 |
| -status <NodeId> | 输出此节点的状态信息                                          |

# application 应用相关

- 输出帮助信息
```shell
yarn application -help
```
- 用法 `yarn application <options>`

  其中 **\<options\>** 可以为：

| options                       | 功能描述                                                |
| ----------------------------- | ------------------------------------------------------- |
| -appStates <States>           | 与 -list 一起使用，可按程序状态过滤应用程序，多个状态按逗号分隔。下列状态是有效的：`ALL`, `NEW`, `NEW_SAVING`, `SUBMITTED`, `ACCEPTED`, `RUNNING`, `FINISHED`, `FAILED`, `KILLED` |
| -appTypes <Types>             | 与 -list 一起使用，可按程序类型过滤应用程序，多个类型按逗号分隔。|
| -help                         | 显示此帮助文档。                                        |
| -kill <Application ID>        | 杀死应用程序。                                          |
| -list                         | 列出应用程序。</br>支持使用 -appTypes 和 -appStates 根据应用程序类型和状态过滤应用。|
| -movetoqueue <Application ID> | 将应用程序移到其他队列。                               |
| -queue <Queue Name>           | 与 -movetoqueue 一起使用可以将应用程序移到指定的队列。 |
| -status <Application ID>      | 输出应用程序的运行状态。                               |

# applicationattempt

- 输出帮助信息
```shell
yarn applicationattempt -help
```
- 用法 `yarn applicationattempt <options>`

  其中 **\<options\>** 可以为：

| options                          | 功能描述                 |
| -------------------------------- | ------------------------ |
| -help                            | 显示此帮助文档。         |
| -list <Application ID>           | 列出应用程序请求。       |
| -status <Application Attempt ID> | 输出应用程序请求的状态。 |

# container 容器相关

- 输出帮助信息
```shell
yarn container -help
```
- 用法 `yarn applicationattempt <options>`

  其中 **\<options\>** 可以为：

| options                        | 功能描述                 |
| ------------------------------ | ------------------------ |
| -help                          | 显示此帮助文档。         |
| -list <Application Attempt ID> | 列出应用容器请求。       |
| -status <Container ID>         | 输出应用容器请求的状态。 |

# queue 队列相关

- 输出帮助信息
```shell
yarn queue -help
```
- 用法 `yarn queue <options>`

  其中 **\<options\>** 可以为：

| options              | 功能描述             |
| -------------------- | -------------------- |
| -help                | 显示此帮助文档。     |
| -status <Queue Name> | 列出指定队列的信息。 |

# logs 日志相关

检索完整的YARN应用程序的日志。

- 输出帮助信息
```shell
yarn logs
```
- 用法 `yarn logs -applicationId <application ID> [OPTIONS]`

  其中 **\<options\>** 可以为：

| options                       | 功能描述                                        |
| ----------------------------- | ----------------------------------------------- |
| -appOwner <Application Owner> | 应用所有者（如果未指定，则为当前用户）          |
| -containerId <Container ID>   | 容器 ID（如果指定了节点地址，则必须指定此选项） |
| -nodeAddress <Node Address>   | 节点地址，格式为 **nodename:port**（如果指定了容器 ID，则必须指定此选项） |


# daemonlog 守护进程日志相关

- 输出帮助信息
```shell
yarn daemonlog
```

- 用法 `yarn daemonlog <options>`

  其中 **\<options\>** 可以为：
```
[-getlevel <host:port> <classname> [-protocol (http|https)]
[-setlevel <host:port> <classname> <level> [-protocol (http|https)]
```

# top 监控器相关
yarn top 是可帮助群集管理员更好地了解群集使用情况的一种工具。

![yarn top](/imgs/截图_20191029_151252.png)

- 输出帮助信息
```shell
yarn top --help
```
- 用法 : yarn top [COMMAND]

  其中 **COMMAND** 可以为：

| COMMAND         | 功能                                                           |
| --------------- | -------------------------------------------------------------- |
| -cols \<arg\>   | 显示在终端上的列数。                                           |
| -delay \<arg\>  | 刷新延迟（以秒为单位），默认为3秒。                            |
| -help           | 显示此帮助文档。;</br>在工具运行时查看帮助，请按`'h'+ Enter`。 |
| -queues \<arg\> | 查看指定队列的应用，多个队列名用逗号分隔。                     |
| -rows \<arg\>   | 显示在终端上的行数。                                           |
| -types \<arg\>  | 查看指定类型的应用，多个类型用逗号分隔。</br>区分大小写（即使显示为小写） |
| -users \<arg\>  | 查看指定用户的应用，多个用户用逗号分隔。                       |

- 各列的含义

| 列名          | 含义                                      |
| ------------- | ----------------------------------------- |
| APPLICATIONID | 应用 ID                                   |
| USER          | 执行此应用的用户名                        |
| TYPE          | 应用类型，可能是 mapreduce、tez、spark 等 |
| QUEUE         | 应用所在的队列                            |
| #CONT         |                                           |
| #RCONT        | 预留容器                                  |
| VCORES        | 虚拟核心                                  |
| RVCORES       | 预留的虚拟核心                            |
| MEM           | 占用内存                                  |
| RMEM          | 预留的内存                                |
| VCORESECS     |                                           |
| MEMSECS       |                                           |
| %PROGR        | 进度百分比                                |
| TIME          | 运行时长，最小单位为分钟                  |
| NAME          | 应用名                                    |

## yarn top 注意事项：

1. 对于RM，获取所有应用程序的信息的资源开销较大。为了防止性能下降，无论刷新延迟值的秒数如何，结果都会缓存 5 秒以指定的延迟间隔获取有关 NodeManager 和队列利用率统计信息的信息。一旦我们对性能影响有了更好的了解，可能会改变这一情况。
2. 由于该工具是用Java实现的，因此必须按Enter键才能处理按键。

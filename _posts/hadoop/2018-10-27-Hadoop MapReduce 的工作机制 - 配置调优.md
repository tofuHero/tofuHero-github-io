---
title: Hadoop MapReduce 的工作机制 - 配置调优
date: 2018-10-27
categories:
- Hadoop
tags:
- Java
- Hadoop
- MapReduce
- shuffle
---

> 内容摘选自 《Hadoop 权威指南 第四版》 7.3.3 配置调优

# MapReduce 的配置调优

- 总的原则是给 shuffle 过程尽量多的提供内存空间
- 确保 map 函数和 reduce 函数得到足够的内存运行
- 写 map 函数和 reduce 函数时尽量少用内存

<!-- more -->

## map 端

属性名称|类型|默认值|说明
-------|---|-----|---
`mapreduce.task.io.sort.mb`|int|100|排序 map 输出时所使用的内存缓冲区大小，以兆字节为单位
`mapreduce.map.sort.spill.percent`|float|0.80|map 输出内存缓冲区和用来开始磁盘溢出写过程的记录边界索引，这两者使用比例的阀值（写入环形缓冲区到 **80%** 时开始溢写）
`mapreduce.task.io.sort.factor`|int|10|排序文件时，一次最多合并的流数。这个属性也在 reduce 中使用。将此值增加到 100 是很常见的
`mapreduce.map.combine.minspills`|int|3|运行 combine 所需的最少溢出文件数（如果已指定 combine）
`mapreduce.map.output.compress`|Boolean|false|是否压缩 map 输出
`mapreduce.map.output.compress.codec`|Class name|org.apache.<br>hadoop.io.<br>compress.<br>DefaultCodec|用于 map 输出的压缩编码器
`mapreduce.shuffle.max.threads`|int|0|每个节点管理器的工作线程数，用于将 map 输出到 reducer。这是集群范围的设置，不能由单个作业设置。0 表示使用 Netty 默认值，可用的处理器两倍的数

## reduce 端

属性名称|类型|默认值|说明
-------|---|-----|---
`mapreduce.reduce.shuffle.parallelcopies`|int|5|用于把 map 输出复制到 reducer 的线程数
`mapreduce.reduce.shuffle.maxfetchfailures`|int|10|在声明失败之前，reducer 获取一个 map 输出所花的最大时间
`mapreduce.task.io.sort.factor`|int|10|排序文件时一次最多合并的流的数量，这个属性也在 map 端使用
`mapreduce.reduce.shuffle.input.buffer.percent`|float|0.70|在 shuffle 的复制阶段，分配给 map 输出的缓冲区占堆空间的百分比
`mapreduce.reduce.shuffle.merge.percent`|float|0.66|map 输出缓冲区(由 mapred.job.shuffle.buffer.percent 定义)的阀值使用比例，用于启动合并输出和磁盘溢出的过程
`mapreduce.reduce.merge.inmem.threshold`|int|10000|启动合并输出和磁盘移除写过程的 map 输出的阀值数。0 或更小的数意味着没有阀值限制，溢出写行为由 mapreduce.reduce.shuffle.merge.percent 单独控制
`mapreduce.reduce.input.buffer.percent`|float|0.0|在 reduce 过程中，在内存中保存 map 输出的空间占整个堆空间的比例。reduce 阶段开始时，内存中的 map 输出大小不能大于这个值。默认情况下，在 reduce 任务开始之前，所有 map 输出都合并到磁盘上，以便为 reducer 提供尽可能多的内存。然而，如果 reducer 需要的内存较少，可以增加此值来最小化访问磁盘次数

---
title: 简述 Shell
date: 2019-07-19
categories:
- Linux
tags:
- Shell
- Bash
- Linux
---

Shell 是什么？“Shell 就是壳。” 这句话相信大多数接触过 Linux / Mac OS 的用户都听过，那怎么去理解呢？

我来打个比方，如果把电视看作是一个操作系统，那么电视壳（包括屏幕、按钮、各种插孔）就是 Shell，内部（电路板以及各种元器件）就是 Linux 内核。

<!-- more -->

![老式电视][1]

可以看到一个电视的壳（Shell）提供了旋钮、按钮（操作方式），我们可以通过这些操作方式去操作内部的元器件（内核），而内部的最终输出可以通过显示屏、音响反馈给我们，这就是壳（Shell）的作用：**提供一种用户与内核交互的方式**

电视有成百上千种，Shell 有没很多种呢？答案是肯定的。常见的 Shell 有 Bash、Dash、Fish、Zsh 等



下面是一份 Unix Shell 的表格

| Shell 名称 | 发行年份 | 作为默认 Shell 的发行版      |
| -------- | ---- | -------------------- |
| Bash     | 1989 | Debian、Ubuntu、RedHat 等 |
| Bourne   | 1979 | Unix 7               |
| Almquist |      |                      |
| csh      |      |                      |
| dash     |      |                      |
| fish     |      |                      |
| Hamilton |      |                      |
| Korn     |      |                      |
| PWB      |      |                      |
| psh      |      |                      |
| Rc       |      |                      |
| sash     |      |                      |
| tcsh     |      |                      |
| Thompson |      |                      |
| Wish     |      |                      |
| zsh      |      |                      |

[1]: /imgs/TV.svg

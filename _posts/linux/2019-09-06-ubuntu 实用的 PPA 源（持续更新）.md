---
title: ubuntu 实用的 PPA 源（持续更新）
date: 2019-09-06
categories:
- Linux
tags:
- Ubuntu
- Linux
---

收录一些自用、实用的 ubuntu 第三方软件 ppa，不定期更新

<!-- more -->

# 系统类

## [NVIDIA 显卡驱动][1]
`ppa:graphics-drivers/ppa`

拥有最新的驱动包，以及大量驱动版本，比起使用官网的安装文件要方便一些

```shell
sudo add-apt-repository ppa:graphics-drivers/ppa
sudo apt-get update
```

其对应的开发版本为 `ppa:graphics-drivers/dev` 和大多数一样，开发版会有一些不稳定，不建议日常工作使用

## [Grub 引导编辑器][2]
`ppa:danielrichter2007/grub-customizer`

```shell
sudo add-apt-repository ppa:danielrichter2007/grub-customizer
sudo apt-get update
```

## [apt-fast][3] : 快速的 apt-get
`ppa:apt-fast/stable`

apt-fast 将下载软件包这一步操作交给强大的多线程下载器 Aria2c 来处理，从而提高整体运行速度

```shell
sudo add-apt-repository ppa:apt-fast/stable
sudo apt-get update
```

## [KDE Plasma 桌面环境](https://launchpad.net/~kubuntu-ppa/+archive/ubuntu/backports)
`ppa:kubuntu-ppa/backports`

```shell
sudo add-apt-repository ppa:kubuntu-ppa/backports
sudo apt-get update
```

Kubuntu、Plasma 和 KDE 应用程序的主要更新途径，这里的更新频率比 Kubuntu 内置的更新频率更高，对待新 bug 处理的更加积极

------------------------------------------------------

# 开发类

## [Oracle Java 安装][4]
~~`ppa:webupd8team/java`~~(已失效)

一般动手能力强点的都去自己使用压缩包的安装方法，手动配置环境变量去了，这个 PPA 可以自动下载压缩包，并自动配置变量，效果一样

```shell
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
```

------------------------------------------------------

# 图像类

## [Krita][5] : 插画 / 漫画 编辑器
`ppa:kritalime/ppa`

虽然说 Ubuntu 的软件源已经存在 Krita 了，但是版本较老，想要使用最新的版本可以添加这个源，并且执行 **更新/安装** 即可

```shell
sudo add-apt-repository ppa:kritalime/ppa
sudo apt-get update
```

------------------------------------------------------

# 美化类

## [Papirus][6] : 图标 + 主题
`ppa:papirus/papirus`

很漂亮的主题（自认为），该 PPA 源包含多种图标主题方案，可以自由搭配

```shell
sudo add-apt-repository ppa:papirus/papirus
sudo apt-get update
```

**安装：**`sudo apt-get install --install-recommends adapta-kde`

------------------------------------------------------

# 游戏类

## [RetroArch][7] : 模拟器合集
`ppa:libretro/stable`

一个模拟器集合，可以兼容绝大部分平台的游戏包括但不限于 NES/FC、SNES、N64、Wii、PS、PS2、PSP，拥有漂亮的界面，非常适合 DIY 家庭主机

```shell
sudo add-apt-repository ppa:libretro/stable
sudo apt-get update
```

**安装：**

```shell
git clone https://github.com/libretro/RetroArch.git retroarch
apt-get build-dep retroarch
```


## [GameHub](https://tkashkin.tk/projects/gamehub/) : 一站式 linux 游戏集中营
`ppa:tkashkin/gamehub`

GameHub 致力于打造一款 Linux 游戏启动平台，现支持：
- Wine / Proton （Windows 游戏）
- DOSBox
- RetroArch
- ScummVM


可以托管以下平台游戏
- Steam
- GOG
- Humble Bundle
- Humble Trove

```shell
sudo add-apt-repository ppa:tkashkin/gamehub
sudo apt update
sudo apt install com.github.tkashkin.gamehub
```

------------------------------------------------------

# 其他类

[1]: https://launchpad.net/~graphics-drivers/+archive/ubuntu/ppa

[2]: https://launchpad.net/~danielrichter2007/+archive/ubuntu/grub-customizer

[3]: https://launchpad.net/~apt-fast/+archive/ubuntu/stable

[4]: https://launchpad.net/~webupd8team/+archive/ubuntu/java

[5]: https://krita.org/zh/download-zh/krita-desktop-zh/

[6]: https://launchpad.net/~papirus

[7]: https://docs.libretro.com/development/retroarch/compilation/ubuntu/
